# NLP-ROBOT : The team of NLP and Speach Recognition by RAS

# DISCLAMER: Este proyecto es demorado, podría tomar hasta 3 años.

- Se espera en los proximos 4 meses haber implmentado las soluciones más rapidas encontradas en internet, en los libros. Tambien se espera haber trabajado en almenos 1 solucion de forma más especifica. 

- De aqui a 1 año se espera haber sentado las bases del arte en Redes neuronales/ Deep Learning , que permitan crear modelos inteligentes SENCILLOS pero teoricamente poderosos, para enfrentar tareas sencillas como Reconocimiento del Lenguaje, Procesamiento de las palabras, frases, textos, Creacion de textos y conversaciones. Hacer que los robots puedan prestar atencion a las conversaciones utilizando Transformers.

- De aqui a 1 año se espera dominar el tema de redes neuronales recurrentes y los transformers.

- De aqui a 2 años se espera haber podido entrener almenos 1 modelo de Deep Learning de alta demanda computacional, ya sea con recursos de la UNiversidad o con recursos de Google Collab, Y habernos enfrentado al reto de entrenar modelos de muchos datos.

-De aqui a 2 años esperamos haber sentado las bases necesarias para crear modelos de AI sobre el NLP. 

.... Todas estas son suposciciones ......

# @willandru: Data Scientist - Bioengineer - Network Engineer : Pontificia Universidad Javeriana
Hola¡ .. Bienvenido a este recorrido, en el que documentaremos desde cero el camino que seguiremos para aprender NLP, ROS, Robotics, AI, Machine Learning, Deep Learning, todo con el objetivo de crear el prototipo de un sistema inteligente capaz de comunicarse con la gente de la Universidad y utilizar a los robots como guias turisticos y de información dentro de la universidad. 

# Dos Tareas en Paralelo: Investigacion e Implementacion

We are basically gonna learn over the next months how the machine learning/deep learning models are created. For example, Facebook has been developing a very powerful model called BlenderBot, Google has developed Deep Learning models usign TRANSFORMERS neural networks, something too powerfull that we will try to recreate a TRANSFORMER in the next months. Another powerful models are GPT3 and GPT4 which are actual open source deep learning models created by OpenAi that have been trained whit over 3000Trillion parameters to create chatbots that outperform anything seen before. 

Asi que queremos iniciar con las soluciones más sencillas, viables y disponibles que tengamos, queremos ir explorando e implmentando todas las soluciones pero ir progresando hacia las redes neuronales y los modelos de Inteligencia Artificial, pero principalmente queremos crear una base del conocimiento y pilares fuertes sobre el entendimiento del NLP y el DEEP LEARNING, para que en unos años podamos dominar este tema.

# MISION: Implementar un sistema de reconocimiento y procesamiento del lenguaje para los robots de la universidad Javeriana. 

Los robots Nao, Pepper , Baxter y Turtle del piso 13 de INgeniería necesitan un sistema inteligente que les ayude a servir de guias turisticos o de información dentro del edificio de INgeniería.

# Objetivos 
1. Implementar todas las soluciones viables existentes sobre NLP y Speeach Recognition, por más simples o complejas que sean, se busca crear una base del conocimiento lo más grande y ordenada posible.

2. Implementar soluciones de Speeach Recognition , Text Processing, NLP y cualquier otro tema que pueda servir para hacer a los Robots hablar y entender el lenguaje.

3. Se quiere encontrar todos los Papers claves que sirvan para recostruir una linea del tiempo sobre las técnicas utilizadas más recientemente de MachineLearning y Deep Learning, para que esto nos sirva como pilar del conocimiento de las ideas de nuestro grupo.



# ¿Qué estoy haciendo actualmente?
- Estoy siguiendo el capitulo 8: Working with Speech REcognition and Synthesis Using Python and ROS

- Estoy realizando una busqueda en internet (videos de youtube sobre NLP y Deep Learning, paginas Web), busqueda de papers y artículos científicos sobre NLP, redes neuronales y como implementar algun paper, estoy mirando en los LIBROS sobre módulos de PYTHON para Speach Recognition y como integrarlo con ROS.

Tengo en mente:

    ##  Speeach Recognition: 
    - VOSK (python module)
    - POCKET SPHINIX (python module)

    ## NLP
    - GPT3
    - Blender Boot
    - Transformers


# Material que estoy utilizando

![alt text](http://www.ros.org/news/assets_c/2015/06/final_cover-01-thumb-640x380-1176.png)


